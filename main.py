from functools import reduce


def modular_inverse(a, z):
    for i in range(z):
        inv_a = a * i % z
        if inv_a == 1:
            return i

    raise Exception('Result not found')


def chinese_remainder_gauss(a, n):
    result = 0
    N = reduce(lambda x, y: x * y, list(n), 1)

    for i in range(len(n)):
        ai = a[i]
        ni = n[i]
        bi = N // ni

        result += ai * bi * modular_inverse(bi, ni)

    return result % N, N


if __name__ == '__main__':
    try:
        a_raw = input("List of a: ")
        n_raw = input("List of n: ")
        a = list(map(lambda a_txt: int(a_txt), a_raw.split(",")))
        n = list(map(lambda n_txt: int(n_txt), n_raw.split(",")))
        result, n_product = chinese_remainder_gauss(a, n)
        print(f'Result of congruence equation: {result}, product of n: {n_product}')
    except Exception as e:
        raise e
