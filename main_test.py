import unittest

import main


class TestSolveEquations(unittest.TestCase):

    def test_congruence_equations(self):
        self.assertEqual(main.chinese_remainder_gauss([6, 7, 4, 7], [7, 13, 5, 11]), (1294, 5005))
        self.assertEqual(main.chinese_remainder_gauss([3, 4, 1], [4, 5, 7]), (99, 140))
        self.assertEqual(main.chinese_remainder_gauss([6, 12, 4, 10], [7, 13, 5, 11]), (5004, 5005))


if __name__ == '__main__':
    unittest.main()
